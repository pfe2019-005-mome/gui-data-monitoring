import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';

export const URL_LOCAL_SERVER = 'http://localhost:3000';
export const URL_MOCK_SERVER = 'api';

@Injectable({providedIn: 'root'})
export class MySocket extends Socket {
  constructor() {
    super({ url: URL_LOCAL_SERVER, options: {} });
  }
}
