import { Injectable } from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {MySocket, URL_LOCAL_SERVER} from '../utils/constants';
import {Sensor} from '../models/sensor';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  mySocket: MySocket;

  constructor() {
    this.mySocket = new MySocket();
  }

  getNbrClientConnected() {
    return this.mySocket.fromEvent<number>('nbr-client-connected').pipe(
      map(response => response as number),
      catchError(() => throwError(`resource under url ${URL_LOCAL_SERVER}$ and resource 'nbr-client-connected' not found`)));
  }

  getDataSensor(placeImus: string) {
    return this.mySocket.fromEvent<Sensor>(placeImus).pipe(
      map(response => response as Sensor),
      catchError(() => throwError(`resource under url ${URL_LOCAL_SERVER}$ and resource 'shoulder-imu' not found`)));
  }
}
