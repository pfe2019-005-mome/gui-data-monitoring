import {Component, OnInit} from '@angular/core';
import {ServerService} from './services/server.service';
import {PlaceImus} from './models/placeImus';
import {Sensor} from './models/sensor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  nbrClientConnected: number;
  DataShoulderSensor: Sensor;
  DataElbowSensor: Sensor;
  DataWristSensor: Sensor;
  DataSwordSensor: Sensor;

  constructor(private serverService: ServerService) {
  }

  ngOnInit() {
    this.serverService.getNbrClientConnected().subscribe(
      nbrClientConnected => this.nbrClientConnected = nbrClientConnected);
    this.serverService.getDataSensor(PlaceImus.SHOULDER_IMU.toString()).subscribe(
      DataShoulderSensor => this.DataShoulderSensor = DataShoulderSensor);
    this.serverService.getDataSensor(PlaceImus.ELBOW_IMU.toString()).subscribe(
      DataElbowSensor => this.DataElbowSensor = DataElbowSensor);
    this.serverService.getDataSensor(PlaceImus.WRIST_IMU.toString()).subscribe(
      DataWristSensor => this.DataWristSensor = DataWristSensor);
    this.serverService.getDataSensor(PlaceImus.SWORD_IMU.toString()).subscribe(
      DataSwordSensor => this.DataSwordSensor = DataSwordSensor);
  }
}
