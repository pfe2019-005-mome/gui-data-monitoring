export class Sensor {
  timestamp: number;
  fusionPose: {x: number, y: number, z: number};
  gyro: {x: number, y: number, z: number};
  accel: {x: number, y: number, z: number};
  compass: {x: number, y: number, z: number};
  rate: number;
}
