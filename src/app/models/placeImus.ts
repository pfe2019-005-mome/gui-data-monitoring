export enum PlaceImus {
  SHOULDER_IMU = 'shoulder-imu',
  ELBOW_IMU = 'elbow-imu',
  WRIST_IMU = 'wrist-imu',
  SWORD_IMU = 'sword-imu'
}
