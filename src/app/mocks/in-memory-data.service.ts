import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const arduinos = [
      { name: 'Arduino 1', imu: {id: '1'} },
      { name: 'Arduino 2', imu: {id: '2'} },
      { name: 'Arduino 3', imu: {id: '3'} },
      { name: 'Arduino 4', imu: {id: '4'} },
    ];
    const imus = [
      { id: 1, data: '[13, 34, 23, 94, 30, 30, 27, 83, 30]' },
      { id: 2, data: '[13, 34, 23, 94, 30, 30, 27, 83, 30]' },
      { id: 3, data: '[13, 34, 23, 94, 30, 30, 27, 83, 30]' },
      { id: 4, data: '[13, 34, 23, 94, 30, 30, 27, 83, 30]' }
    ];
    return {arduinos, imus};
  }
}
